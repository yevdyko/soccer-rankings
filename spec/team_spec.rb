require 'team'

describe Team do
  subject(:team) { described_class.new(name, score) }

  let(:name)  { 'Lions' }
  let(:score) { 3 }

  context '#initialize' do
    it { is_expected.to respond_to(:name) }
    it { is_expected.to respond_to(:score) }
    it { is_expected.to respond_to(:points) }

    it 'has a name' do
      expect(team.name).to eq(name)
    end

    it 'removes trailing whitespaces from name' do
      team_1 = described_class.new('Lions ', 1)

      expect(team_1.name).to eq('Lions')
    end

    it 'has a score' do
      expect(team.score).to eq(score)
    end

    it 'has a score as integer' do
      team_1 = described_class.new(name, '3')

      expect(team_1.score).to be_a_kind_of(Numeric)
    end

    it 'has points of zero by default' do
      expect(team.points).to eq(0)
    end
  end

  context '#add' do
    it { is_expected.to respond_to(:add).with(1) }

    it 'increments the team points' do
      expect { team.add(3) }.to change { team.points }.by 3
    end
  end
end
