require 'game'
require 'team'

describe Game do
  subject(:game) { described_class.new(team_1, team_2) }

  let(:team_1) { instance_double('Team', points: 0) }
  let(:team_2) { instance_double('Team', points: 0) }

  context '#initialize' do
    it { is_expected.to respond_to(:team_1) }
    it { is_expected.to respond_to(:team_2) }

    it 'has an instance of team one ' do
      expect(game.team_1).to eq(team_1)
    end

    it 'has an instance of team two' do
      expect(game.team_2).to eq(team_2)
    end
  end
end
