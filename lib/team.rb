class Team
  attr_accessor :score, :points
  attr_reader :name

  def initialize(name, score)
    @name = name.strip
    @score = score.to_i
    @points = 0
  end

  def add(points)
    @points += points
  end
end
