class Game
  DRAW_POINTS = 1
  WIN_POINTS = 3

  attr_reader :team_1, :team_2

  def initialize(team_1, team_2)
    @team_1 = team_1
    @team_2 = team_2
  end

  def calculate_results
    if team_1.score == team_2.score
      team_1.points += DRAW_POINTS
      team_2.points += DRAW_POINTS
    elsif team_1.score > team_2.score
      team_1.points += WIN_POINTS
    else
      team_2.points += WIN_POINTS
    end
  end
end
