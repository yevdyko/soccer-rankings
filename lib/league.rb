require_relative 'game'
require_relative 'team'
require_relative 'ranking'

class League
  attr_accessor :games, :lines, :ranking

  def self.run(file_path)
    lines = File.open(file_path, 'rb').read
    new(lines).run
  end

  def initialize(lines)
    @games = []
    @lines = lines
    @ranking = Ranking.new
  end

  def run
    lines = parse_lines
    build_games(lines)
    ranking.add_teams(games)
    ranking.teams = rank(ranking.teams)
    ranking.sort_teams_by_points_and_name
    ranking.display_results
  end

  private

  def parse_lines
    lines.each_line.map(&:chop)
  end

  def build_games(lines)
    lines.each do |line|
      teams = line.split(',')
      name_and_score_1 = teams[0]
      name_and_score_2 = teams[1]
      name_1 = select_name(name_and_score_1)
      name_2 = select_name(name_and_score_2)
      score_1 = select_score(name_and_score_1)
      score_2 = select_score(name_and_score_2)
      team_1 = Team.new(name_1, score_1)
      team_2 = Team.new(name_2, score_2)
      game = Game.new(team_1, team_2)
      game.calculate_results
      games << game
    end
  end

  def select_name(name_and_score)
    name_and_score[0..-2]
  end

  def select_score(name_and_score)
    name_and_score[-1]
  end

  def rank(teams)
    teams.reduce([]) do |result, team|
      repeated_team = result.select { |n| n.name == team.name }
      if repeated_team.any?
        result.each { |n| n.add(team.points) if n.name == team.name }
      else
        result << team
      end
    end
  end
end
