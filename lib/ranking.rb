class Ranking
  attr_accessor :teams

  def initialize
    @teams = []
  end

  def add_teams(games)
    games.each do |game|
      self.teams << game.team_1
      self.teams << game.team_2
    end
  end

  def sort_teams_by_points_and_name
    teams.sort! do |team_1, team_2|
      if team_1.points == team_2.points
        team_1.name <=> team_2.name
      else
        team_2.points <=> team_1.points
      end
    end
  end

  def display_results
    teams.map.with_index(1) do |team, index|
      puts "#{index}. #{team.name}, #{team.points} #{select_endings(team)}"
    end.join('\n')
  end

  private

  def select_endings(team)
    team.points > 1 ? 'pts' : 'pt'
  end
end
