# Soccer Rankings

[![Build Status](https://travis-ci.org/yevdyko/soccer-rankings.svg?branch=master)](https://travis-ci.org/yevdyko/soccer-rankings) [![Coverage Status](https://coveralls.io/repos/github/yevdyko/soccer-rankings/badge.svg?branch=master)](https://coveralls.io/github/yevdyko/soccer-rankings?branch=master)

A command-line application that calculates the ranking table for a soccer league.

## Instalation

If you don't have Bundler already, run the command:

    $ gem install bundler

Install all dependencies with:

    $ bundle install

## Testing

To run the tests:

    $ rspec

## How to use

To see the code in action, run from the root directory of this project:

    $ ruby run.rb "./data/sample-input.txt"
